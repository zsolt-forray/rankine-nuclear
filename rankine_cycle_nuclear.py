#!/usr/bin/python3


"""
Calculates the efficiency of Reversible Rankine Cycle
Nuclear Power Plant with or without moisture separator
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '25/11/2019'
__status__  = 'Development'


import os
import json
from chart_plotting import ChartPlotting


def read_json(filename):
    folder = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(folder, "json", filename)) as f:
        return json.load(f)


class Entropy:
    def s1l(self):
        return self.sat_high_prop[str(self.p1)]["entropy_l"]

    def s1v(self):
        return self.sat_high_prop[str(self.p1)]["entropy_v"]

    def s2l(self):
        return self.sat_low_prop[str(self.p2)]["entropy_l"]

    def s2(self):
        return self.s1v()

    def smv(self):
        return self.sep_prop[str(self.pm)]["entropy2"][-1]

    def sml(self):
        return self.sep_prop[str(self.pm)]["entropy2"][0]


class Enthalpy:
    def h2l(self):
        return self.sat_low_prop[str(self.p2)]["enthalpy_l"]

    def h1v(self):
        return self.sat_high_prop[str(self.p1)]["enthalpy_v"]

    def hm(self):
        s1v = self.s1v()
        ix_s1v = self.sep_prop[str(self.pm)]["entropy2"].index(s1v)
        return self.sep_prop[str(self.pm)]["enthalpy2"][ix_s1v]

    def hmv(self):
        return self.sep_prop[str(self.pm)]["enthalpy2"][-1]

    def hml(self):
        return self.sep_prop[str(self.pm)]["enthalpy2"][0]

    def h2m(self):
        smv = self.smv()
        ix_smv = self.two_phase_prop[str(self.p2)]["entropy2"].index(smv)
        return self.two_phase_prop[str(self.p2)]["enthalpy2"][ix_smv]


class RankineCycleNuclear(Entropy, Enthalpy, ChartPlotting):
    def __init__(self):
        self.p1 = 4.5   # MPa
        self.p2 = 0.006 # MPa
        self.pm = 0.3   # MPa
        self.sep_sel = "No"
        ChartPlotting.__init__(self)

    def read_properties(self):
        self.sat_prop = read_json("saturation_properties.json")
        self.sat_high_prop = read_json("sat_hp_properties.json")
        self.two_phase_prop = read_json("two_phase_properties.json")
        self.sat_low_prop = read_json("sat_lp_properties.json")
        self.sep_prop = read_json("sep_properties.json")

    def select_ts_curve_data(self):
        # data to draw T-s curve
        self.temp = self.sat_prop["temperature"]
        self.sl = self.sat_prop["entropy_l"]
        self.sv = self.sat_prop["entropy_v"]

    def calc_saturated_steam_temperature(self):
        return self.sat_high_prop[str(self.p1)]["sat_temp"]

    def calc_moisture_separator_temperature(self):
        return self.sep_prop[str(self.pm)]["sat_temp"]

    def calc_condenser_temperature(self):
        return self.sat_low_prop[str(self.p2)]["sat_temp"]

    def calc_cycle_points(self):
        # cycle curve data
        i = 0
        ii = 0
        t = self.sat_prop["temperature"][ii]
        t1sat = self.calc_saturated_steam_temperature()
        t2sat = self.calc_condenser_temperature()
        tmsat = self.calc_moisture_separator_temperature()
        while t < t1sat:
            if t > t2sat and i == 0:
                i = ii
            ii += 1
            t = self.sat_prop["temperature"][ii]

        sml = self.sml()
        s1v = self.s1v()
        s2l = self.s2l()
        s1l = self.s1l()
        s2 = self.s2()
        smv = self.smv()

        # separator left line
        self.mx_left = [sml, s1v]
        self.my = [tmsat, tmsat]

        liqt = list(self.sat_prop["temperature"][i:ii])
        liqs = list(self.sat_prop["entropy_l"][i:ii])

        if self.sep_sel == "No":
            cycle_t = [t2sat] + liqt + [t1sat, t1sat, t2sat, t2sat]
            cycle_s = [s2l] + liqs + [s1l, s1v, s2, s2l]

        elif self.sep_sel == "Yes":
            cycle_t = [t2sat] + liqt + [t1sat, t1sat, tmsat, tmsat, t2sat, t2sat]
            cycle_s = [s2l] + liqs + [s1l, s1v, s1v, smv, smv, s2l]

        self.pos_1x = s1v - 1.5
        self.pos_1y = t1sat + 6.0
        self.pos_2x = s2 - 1.5
        self.pos_2y = t2sat + 6.0

        return cycle_s, cycle_t

    def calc_efficiency(self):
        s1v = self.s1v()
        h1v = self.h1v()
        h2l = self.h2l()

        if self.sep_sel == "No":
            # No separator used
            s2l = self.s2l()
            t2sat = self.calc_condenser_temperature()
            # average t1 and efficiency
            t1avg = (h1v - h2l) / (s1v - s2l) # in Kelvin!
            eff = 1 - (t2sat+273.15) / t1avg

        elif self.sep_sel == "Yes":
            # Separator used
            sml = self.sml()
            smv = self.smv()
            hm = self.hm()
            hmv = self.hmv()
            h2m = self.h2m()
            hml = self.hml()
            # steam value and efficiency
            x = (s1v - sml) / (smv - sml)
            eff = ((h1v - hm) + x * (hmv - h2m)) \
                    / (x * (h1v - h2l) + (1-x) * (h1v - hml))
        return eff

    def run_calc(self):
        self.read_properties()
        self.select_ts_curve_data()
        self.cycle_s, self.cycle_t = self.calc_cycle_points()
        self.eff = self.calc_efficiency()
        return self.cycle_s, self.cycle_t, self.eff


def run_app():
    rcobj = RankineCycleNuclear()
    rcobj.run_calc()
    rcobj.run_plot()


if __name__ == "__main__":
    run_app()
