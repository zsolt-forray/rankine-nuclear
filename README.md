# Rankine Cycle - Nuclear PP

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d43c1aaafab241048129360037a80296)](https://www.codacy.com/manual/forray.zsolt/rankine-nuclear?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zsolt-Forray/rankine-nuclear&amp;utm_campaign=Badge_Grade)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
This tool calculates the efficiency of Reversible Rankine Cycle for Nuclear Power Plant. As an option the use of moisture separator can be selected.

## Usage

![Screenshot](/png/fig.png)

### Usage Example

```python
#!/usr/bin/python3

import rankine_cycle_nuclear as rcn

rcn.run_app()
```

**Parameters:**

*	p1: Pressure (MPa) - Isobar Heat Transfer
*	p2: Pressure (MPa) - Isobar Heat Rejection
*	pm: Pressure (MPa) - Working pressure of Moisture Separator

## Contributions
Contributions to this repository are always welcome.
This repo is maintained by Zsolt Forray (forray.zsolt@gmail.com).
