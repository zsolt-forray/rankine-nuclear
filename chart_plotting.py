#!/usr/bin/python3


"""
Chart plotting
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '09/12/2019'
__status__  = 'Development'


from matplotlib import pyplot as plt
from chart_settings import ChartSettings


class ChartPlotting(ChartSettings):
    def __init__(self):
        ChartSettings.__init__(self)

    def run_plot(self):
        self.set_chart()
        self.ax.set_ylim(0, 600)
        self.ax.set_xlim(0, 10)
        self.ax.plot(self.sl, self.temp, color="k", linewidth=0.7)
        self.ax.plot(self.sv, self.temp, color="k", linewidth=0.7)
        self.l, = self.ax.plot(self.cycle_s, self.cycle_t, color="r", linewidth=1.5)
        self.ll, = self.ax.plot(self.mx_left, self.my, color="w", linewidth=0.0)

        self.button_inc_p1.on_clicked(self.inc_p1)
        self.button_dec_p1.on_clicked(self.dec_p1)
        self.button_inc_p2.on_clicked(self.inc_p2)
        self.button_dec_p2.on_clicked(self.dec_p2)
        self.button_inc_pm.on_clicked(self.inc_pm)
        self.button_dec_pm.on_clicked(self.dec_pm)
        self.button_close.on_clicked(ChartPlotting.close_chart)

        self.radio.on_clicked(self.separator_selector)

        self.pos_1 = self.ax.annotate(" p1, t1s", (self.cycle_s[30]-1.5, self.cycle_t[30]+6.0))
        self.pos_2 = self.ax.annotate(" p2, t2s", (self.cycle_s[31]-1.5, self.cycle_t[31]+6.0))
        self.pos_m = self.ax.annotate(" pm", (self.mx_left[1]-1.5, self.my[1]+6.0), color="w")

        plt.show()

    def inc_p1(self, evt):
        if self.p1 < 6.0:
            self.p1 += 0.5
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p1_text.set_text("{}MPa".format(self.p1))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.ll.set_xdata(self.mx_left)
        self.ll.set_ydata(self.my)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.pos_m.set_position((self.mx_left[1]-1.5, self.my[1]+6.0))
        self.fig.canvas.draw_idle()

    def dec_p1(self, evt):
        if self.p1 > 4.5:
            self.p1 -= 0.5
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p1_text.set_text("{}MPa".format(self.p1))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.ll.set_xdata(self.mx_left)
        self.ll.set_ydata(self.my)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.pos_m.set_position((self.mx_left[1]-1.5, self.my[1]+6.0))
        self.fig.canvas.draw_idle()

    def inc_pm(self, evt):
        if self.pm < 0.4:
            self.pm += 0.05
            self.pm = round(self.pm, 2)
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.pm_value_text.set_text("{}MPa".format(self.pm))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.ll.set_xdata(self.mx_left)
        self.ll.set_ydata(self.my)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.pos_m.set_position((self.mx_left[1]-1.5, self.my[1]+6.0))
        self.fig.canvas.draw_idle()

    def dec_pm(self, evt):
        if self.pm > 0.3:
            self.pm -= 0.05
            self.pm = round(self.pm, 2)
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.pm_value_text.set_text("{}MPa".format(self.pm))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.ll.set_xdata(self.mx_left)
        self.ll.set_ydata(self.my)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.pos_m.set_position((self.mx_left[1]-1.5, self.my[1]+6.0))
        self.fig.canvas.draw_idle()

    def inc_p2(self, evt):
        if self.p2 < 0.007:
            self.p2 += 0.001
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p2_text.set_text("{}MPa".format(self.p2))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.ll.set_xdata(self.mx_left)
        self.ll.set_ydata(self.my)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.pos_m.set_position((self.mx_left[1]-1.5, self.my[1]+6.0))
        self.fig.canvas.draw_idle()

    def dec_p2(self, evt):
        if self.p2 > 0.004:
            self.p2 -= 0.001
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p2_text.set_text("{}MPa".format(self.p2))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.ll.set_xdata(self.mx_left)
        self.ll.set_ydata(self.my)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.pos_m.set_position((self.mx_left[1]-1.5, self.my[1]+6.0))
        self.fig.canvas.draw_idle()

    def separator_selector(self, label):
        self.sep_sel = label
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        if self.sep_sel == "Yes":
            self.button_dec_pm.ax.set_position((0.105, 0.29, 0.04, 0.04))
            self.button_inc_pm.ax.set_position((0.165, 0.29, 0.04, 0.04))
            self.pm_label_text.set_visible(True)
            self.pm_value_text.set_visible(True)

            self.l.set_ydata(cycle_t)
            self.l.set_xdata(cycle_s)

            self.ll.set_xdata(self.mx_left)
            self.ll.set_ydata(self.my)
            self.ll.set_linewidth(0.5)
            self.ll.set_color("g")

            self.pos_m.set_color("k")

        elif self.sep_sel == "No":
            self.button_dec_pm.ax.set_position((-0.01, -0.01, 0.001, 0.001))
            self.button_inc_pm.ax.set_position((-0.01, -0.02, 0.001, 0.001))
            self.pm_label_text.set_visible(False)
            self.pm_value_text.set_visible(False)

            self.l.set_ydata(cycle_t)
            self.l.set_xdata(cycle_s)

            self.ll.set_xdata(0.0)
            self.ll.set_ydata(0.0)
            self.ll.set_linewidth(0.0)

            self.pos_m.set_color("w")

        self.fig.canvas.draw_idle()

    @staticmethod
    def close_chart(evt):
        plt.close()
