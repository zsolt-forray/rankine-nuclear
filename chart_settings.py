#!/usr/bin/python3


"""
Chart settings
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '09/12/2019'
__status__  = 'Development'


import os
import json
from matplotlib import pyplot as plt
from matplotlib.widgets import Button, RadioButtons


class ChartSettings:
    def __init__(self):
        # JSON file location
        self.json_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), "json")
        # Label axes position
        self.text_axes_pos = (0.055, 0.15, 0.5, 0.1)
        # Chart labels
        self.titles = { "title" : "Nuclear Power Plant Cycle",
                        "ylabel": "Temperature, °C",
                        "xlabel": "Entropy, kJ/kgK",
                        }

    @staticmethod
    def set_axes(*pos):
        axes = plt.axes([*pos])
        return axes

    @staticmethod
    def set_buttons_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Button(ChartSettings.set_axes(*positions), params["text"],\
                                    color=params["color"], hovercolor=params["hovercolor"])

    @staticmethod
    def set_radio_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return RadioButtons(ChartSettings.set_axes(*positions), params["text"], params["active"])

    def set_labels_params(self, params, text):
        positions = params["x"], params["y"]
        return self.ax_text.text(*positions, text, size=params["size"],\
                                 style=params["style"], color=params["color"],\
                                 backgroundcolor=params["bgcolor"],\
                                 weight=params["weight"])

    def set_buttons(self):
        # Buttons
        with open(os.path.join(self.json_loc, "button_parameters.json")) as json_file:
            button_params = json.load(json_file)

        # Button for increasing p1
        inc_p1_button_params = button_params["inc_p1"]
        self.button_inc_p1 = ChartSettings.set_buttons_params(inc_p1_button_params)

        # Button for decreasing p1
        dec_p1_button_params = button_params["dec_p1"]
        self.button_dec_p1 = ChartSettings.set_buttons_params(dec_p1_button_params)

        # Button for increasing pm
        inc_pm_button_params = button_params["inc_pm"]
        self.button_inc_pm = ChartSettings.set_buttons_params(inc_pm_button_params)

        # Button for decreasing pm
        dec_pm_button_params = button_params["dec_pm"]
        self.button_dec_pm = ChartSettings.set_buttons_params(dec_pm_button_params)

        # Button for increasing p2
        inc_p2_button_params = button_params["inc_p2"]
        self.button_inc_p2 = ChartSettings.set_buttons_params(inc_p2_button_params)

        # Button for decreasing p2
        dec_p2_button_params = button_params["dec_p2"]
        self.button_dec_p2 = ChartSettings.set_buttons_params(dec_p2_button_params)

        # Button for close
        close_button_params = button_params["close"]
        self.button_close = ChartSettings.set_buttons_params(close_button_params)

    def set_radiobuttons(self):
        # RadioButtons
        with open(os.path.join(self.json_loc, "radio_parameters.json")) as json_file:
            radio_params = json.load(json_file)

        # RadioButtons for moisture separator selector
        moisture_selector_params = radio_params["moisture_selector"]
        self.radio = ChartSettings.set_radio_params(moisture_selector_params)

    def set_labels(self):
        self.ax_text = ChartSettings.set_axes(*self.text_axes_pos)
        self.ax_text.axis("off")

        # Labels
        with open(os.path.join(self.json_loc, "label_parameters.json")) as json_file:
            label_params = json.load(json_file)

        input_label_params = label_params["input_label"]
        self.set_labels_params(input_label_params, "Input parameters")

        p1_label_params = label_params["p1_label"]
        self.set_labels_params(p1_label_params, "p1")

        p1_value_params = label_params["p1_value"]
        p1_text = "{}MPa".format(self.p1)
        self.p1_text = self.set_labels_params(p1_value_params, p1_text)

        separator_label_params = label_params["separator_label"]
        self.set_labels_params(separator_label_params, "Moisture Separator")

        pm_label_params = label_params["pm_label"]
        self.pm_label_text = self.set_labels_params(pm_label_params, "pm")

        pm_value_params = label_params["pm_value"]
        pm_text = "{}MPa".format(self.pm)
        self.pm_value_text = self.set_labels_params(pm_value_params, pm_text)

        p2_label_params = label_params["p2_label"]
        self.set_labels_params(p2_label_params, "p2")

        p2_value_params = label_params["p2_value"]
        p2_text = "{}MPa".format(self.p2)
        self.p2_text = self.set_labels_params(p2_value_params, p2_text)

        result_label_params = label_params["result_label"]
        self.res_text = "Efficiency: {:>16.2%}"
        self.tbox = self.set_labels_params(result_label_params, self.res_text\
                                           .format(self.eff))

        self.pm_label_text.set_visible(False)
        self.pm_value_text.set_visible(False)

    def set_chart(self):
        self.fig = plt.figure(figsize=(9,6))
        self.ax = self.fig.add_subplot(1, 1, 1)
        # General
        self.ax.grid(False)
        plt.title(self.titles["title"])
        plt.ylabel(self.titles["ylabel"])
        plt.xlabel(self.titles["xlabel"])
        plt.subplots_adjust(bottom=0.15, left=0.4, right=0.95)
        # Widgets
        self.set_buttons()
        self.set_radiobuttons()
        self.set_labels()
        return self.fig, self.ax
